<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function Index()
	{
        if (Request::isMethod('post')) {
            $validator = Validator::make(Input::all(), array(
                'email' => 'required|unique:subscribers,email',
            ));

            if ($validator->fails())
                return Redirect::to('/')->withErrors($validator);
            $sub=new Subscriber();
            $sub->email=Input::get('email');
            $sub->save();
        }
        return View::make('landing');
	}

}
